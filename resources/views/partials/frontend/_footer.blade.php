<!-- Footer -->
<footer style="background-color: #2f3030">
    <div class="container d-none d-md-block">
        <div class="row py-5">
            <div class="col-5">
                <p class=" font-montserrat gold txt-footer text-center">© 2019 Manuka Farm Manuka Honey - a Product by Natural Farm.</p>
            </div>
            <div class="col-2 text-center">
                <img src="{{ asset('images/manuka/logo-footer.png') }}" alt="img" class="footer-logo">
            </div>
            <div class="col-5 text-center">
                <a href="https://www.facebook.com/manukafarmID/">
                <img src="{{ asset('images/manuka/fesbuk-manuks.png') }}" alt="img" class="socmed-logo">
                </a>
                <a href="#">
                    <img src="{{ asset('images/manuka/twiter-manuks.png') }}" alt="img" class="socmed-logo">
                </a>
                <a href="https://www.instagram.com/manukafarm/">
                    <img src="{{ asset('images/manuka/insta-manuks.png') }}" alt="img" class="socmed-logo">
                </a>
            </div>
        </div>
    </div>
    <div class="container-fluid d-none d-md-block">
        <div class="row no-gutters py-1" style="background-color: #ceae5e"></div>
    </div>
    <div class="container d-block d-md-none">
        <div class="row  py-5">
            <div class="col-12 text-center">
                <img src="{{ asset('images/manuka/logo-footer.png') }}" alt="img" class="footer-logo">
                <p class="font-montserrat gold txt-footer">© 2019 Manuka Farm Manuka Honey - a Product by Natural Farm.</p>
                <a href="https://www.facebook.com/manukafarmID/">
                    <img src="{{ asset('images/manuka/fesbuk-manuks.png') }}" alt="img" class="socmed-logo">
                </a>
                <a href="#">
                    <img src="{{ asset('images/manuka/twiter-manuks.png') }}" alt="img" class="socmed-logo">
                </a>
                <a href="https://www.instagram.com/manukafarm/">
                    <img src="{{ asset('images/manuka/insta-manuks.png') }}" alt="img" class="socmed-logo">
                </a>
            </div>
        </div>
    </div>
    <div class="container-fluid d-block d-md-none">
        <div class="row no-gutters py-1" style="background-color: #ceae5e"></div>
    </div>
</footer>

<style>

    .container-fluid{
        padding:0px
    }
    .txt-footer{
        padding-top:10%;
        font-size:13px;
    }
    .socmed-logo{
        width:50px;
        padding-right:10px;
        padding-top:7%;
    }

    @media (min-width: 576px){
        .txt-footer{
            font-size:13px;
        }
    }
</style>
