@extends('layouts.frontend')

@section('head_and_title')
    <meta name="description" content="31 SUDIRMAN SUITES Home">
    <meta name="author" content="PT. Generasi Muda Gigih">
    <meta name="keywords" content="31 SUDIRMAN SUITES, Property, Office, Residence, Apartment, House">

    <title>MANUKA FARM</title>
@endsection

@section('content')

    <section class="bg-home">
        <div class="container-fluid d-none d-md-block">
            <div class="row no-gutters">
                <div class="col-7"></div>
                <div class="col-1">
                    <a class="btn btn-lg btn-gold px-4  font-montserrat-semibold btn-banner-home" href="{{ route('frontend.product') }}">SHOP NOW</a>
                </div>
                <div class="col-4"></div>
            </div>
        </div>
    </section>

    <!-- Pure New Zealand Section -->
    <section class="bg-pure font-montserrat">
        <div class="container-fluid">
            <!--  -->
            <div class="row no-gutters pb-md-0">
                <div class="col-md-1"></div>
{{--                <div class="col-md-5 text-md-left text-center padd-pure-fhd">--}}
                <div class="col-md-5 text-md-left text-center">
                    <p class="pb-md-5 pb-3 header-gold font-montserrat">
                        Pure New Zealand<br/>
                        Manuka Honey
                    </p>
                    <p class="txt-body pb-4 px-5 px-md-0">
                        Manuka Farm Manuka Honey is 100% pure New Zealand Manuka Honey with the highest quality. Manuka Honey is a natural honey originating from the unique Manuka flower from New Zealand.
                    </p>
                    <p class="txt-body  pb-4 px-5 px-md-0">
                        Unlike ordinary honey, Manuka Honey contains high anti-bacterial properties. Feel the goodness of New Zealand’s nature with us.
                    </p>
                    <a class="btn btn-gold px-4 font-montserrat-medium" href="{{ route('frontend.about') }}">KNOW MORE ABOUT MANUKA</a>
                </div>
                <div class="col-md-6 text-right d-none d-md-block">
                    <img src="{{ asset('images/manuka/home/01 home - bee.png') }}" alt="img" class="img-pure">
                </div>
            </div>
        </div>
    </section>

    <!-- product Section -->
    <section class="bg-product font-montserrat" style="margin-bottom:-5px;">
        <div class="container py-5">
            <div class="row pb-5 d-block d-md-none px-3">
                <div class="col-12 text-center">
                    <p class="header-gold pb-3">Our Product</p>
                    <p class=" text-white font-weight-bold pb-md-2 pb-3" style="font-size:18px; ">Manuka Farm Grading System - MGS (Molan Gold Standard)</p>
                    <p class="text-white txt-body">is a system assessing the level or levels of Methylglyoxal contained in Manuka Honey
                        to guarantee purity and quality. Molan Gold Standard was created by Dr. Peter Molan, who is one
                        of the pioneers of Manuka Honey and the leading honey expert in the world since 1981.</p>
                </div>
            </div>
            <div class="row  px-5">
                <div class="col-md-4 col-12 text-center pb-3">
                    <div class="pt-3">
                        <img src="{{ asset('images/manuka/home/section-3-1.png') }}" alt="img" class="img-product-home ">
                        <p class="text-white txt-product-home">Manuka Farm<br/>
                            MGS 5+ (250g)
                        </p>
                        <p class="gold txt-body">Maintenance (5+)</p>
                        <div class="border-product-home"></div>
                        <img src="{{ asset('images/manuka/home/antibacterial5.png') }}" class="py-3" alt="">
                        <a class="btn btn-gold px-4 font-montserrat-medium" href="https://naturalfarm.id/product/677/manuka-farm-mgs-5-250g">DETAILS</a>
                    </div>
                </div>
                <div class="col-md-4 col-12 text-center pb-3">
                    <div class="pt-3">
                        <img src="{{ asset('images/manuka/home/section-3-2.png') }}" alt="img" class="img-product-home">
                        <p class="text-white txt-product-home">Manuka Farm<br/>
                            MGS 16+ (250g)
                        </p>
                        <p class="gold txt-body">Specialized (16+)</p>
                        <div class="border-product-home"></div>
                        <img src="{{ asset('images/manuka/home/antibacterial16.png') }}" class="py-3" alt="">
                        <a class="btn btn-gold px-4 font-montserrat-medium" href="https://naturalfarm.id/product/678/manuka-farm-mgs-16-250g">DETAILS</a>
                    </div>
                </div>
                <div class="col-md-4 col-12 text-center">
                    <div class="pt-3">
                        <img src="{{ asset('images/manuka/home/section-3-3.png') }}" alt="img" class="img-product-home">
                        <p class="text-white txt-product-home">Manuka Farm<br/>
                            MGS 20+ (250g)
                        </p>
                        <p class="gold txt-body">Therapeutic (20+)</p>
                        <div class="border-product-home"></div>
                        <img src="{{ asset('images/manuka/home/antibacterial20.png') }}" class="py-3" alt="">
                        <a class="btn btn-gold px-4 font-montserrat-medium" href="https://naturalfarm.id/product/925/manuka-farm-mgs-20-250g">DETAILS</a>
                    </div>
                </div>
            </div>
            <div class="row py-5 d-none d-md-block">
                <div class="col-12 text-center">
                    <p class="txt-body text-white font-weight-bold">Manuka Farm Grading System - MGS (Molan Gold Standard)</p>
                    <p class="text-white txt-body px-md-5 px-2">is a system assessing the level or levels of Methylglyoxal contained in Manuka Honey
                        to guarantee purity and quality. Molan Gold Standard was created by Dr. Peter Molan, who is one
                        of the pioneers of Manuka Honey and the leading honey expert in the world since 1981.</p>
                </div>
            </div>
        </div>
    </section>

    <!-- why manukafarm Section -->
    <section class="bg-whymanuka font-montserrat marbot-5">
        <div class="container text-white ">
            <div class="row px-md-5 px-2">
                <div class="col-12">
                    <p class="header-whymanuka text-center">Why Manuka Farm?</p>
                </div>
            </div>
             <div class="row px-md-5 px-2">
                <div class="col-md-4 col-12 text-center pb-5 ">
                    <div class="pt-3">
                        <img src="{{ asset('images/manuka/home/section-4-1.png') }}" alt="img" class="img-whymanuka">
                        <p class="txt-whymanuka py-4">Highest Quality</p>
                        <p class="txt-body px-3">Manuka Farm Manuka Honey is 100% pure New Zealand Manuka Honey with the highest quality.</p>
                    </div>
                </div>
                <div class="col-md-4 col-12 text-center pb-5">
                    <div class="pt-3">
                        <img src="{{ asset('images/manuka/home/section-4-2.png') }}" alt="img" class="img-whymanuka">
                        <p class="txt-whymanuka py-4">100% Pure New Zealand</p>
                        <p class="txt-body px-3">Manuka Honey Manuka Farm is obtained from
                            one of the most natural and least polluted areas in New Zealand.</p>
                    </div>
                </div>
                <div class="col-md-4 col-12 text-center">
                    <div class="pt-3">
                        <img src="{{ asset('images/manuka/home/section-4-3.png') }}" alt="img" class="img-whymanuka">
                        <p class="txt-whymanuka py-4">Certified & Tested</p>
                        <p class="txt-body px-3">Manuka Farm Manuka Honey has been tested and certified by the world quality standard ISO17025.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Blog list Section -->
    <section class="bg-custom-dark">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-12 mb-3 mb-md-0">
                    <div class="row mb-5">
                        <div class="col-12 text-center pt-5">
                            <div class="pb-1">
                                <span class="font-montserrat text-white text-center t1-m-1" style="font-size: 30px;">Feature Blog and News</span>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-4 text-center" id="list_latest_new">
                        <div class="col-12">
                            <div class="row">
                            @foreach($news as $new)
                                <div class="col-md-4 col-12">
                                    <div class=" pb-5" >
                                        <a href="{{ route('frontend.news_detail', ['slug' => $new->slug]) }}" class="hov-img0 of-hidden">
                                            <img src="{{ asset($new->img_path) }}" alt="IMG" style="width:300px;">
                                        </a><br/>
                                        <span class="pb-1 text-white font-montserrat">{{$new->created_at_front_end_formatted}}</span><br/>
                                        <span class="text-white font-montserrat font-weight-bold">{{ $new->title }}</span><br/>
                                        <a href="{{ route('frontend.news_detail', ['slug' => $new->slug]) }}" class="font-montserrat gold" style="text-decoration: underline;">
                                            READ MORE
                                        </a>
                                    </div>
                                </div>
                            @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>



    <!-- Instagram Section -->
    <section class="bg-custom-dark py-5 d-none d-md-block">
        <div class="container">
            <div class="row px-md-5 px-2">
                <div class="col-12">
                    <p class="header-instagram text-center text-white font-montserrat">Follow Us on Instagram</p>
                </div>
            </div>
            <div class="row mb-4">
                <div class="col-12 text-center">
                    <!-- SnapWidget -->
                    <iframe src="https://snapwidget.com/embed/800057" class="snapwidget-widget" allowtransparency="true" frameborder="0" scrolling="no" style="border:none; overflow:hidden;  width:990px; height:330px"></iframe>
                </div>
            </div>
        </div>
    </section>

    <!-- Instagram Section mobile -->
    <section class="bg-custom-dark py-5 d-block d-md-none">
        <div class="container">
            <div class="row px-md-5 px-2">
                <div class="col-12">
                    <p class="header-instagram text-center text-white font-montserrat">Follow Us on Instagram</p>
                </div>
            </div>
            <div class="row mb-4">
                <div class="col-12 text-center">
                    <!-- SnapWidget -->
                    <iframe src="https://snapwidget.com/embed/800059" class="snapwidget-widget" allowtransparency="true" frameborder="0" scrolling="no" style="border:none; overflow:hidden;  width:250px; height:625px"></iframe>
                </div>
            </div>
        </div>
    </section>


{{--    <div>--}}
{{--    <a href="#" class="float">--}}
{{--            <img src="{{ asset('images/31ss/wa-logo-thumb.png') }}" alt="IMG" class="fa my-float w-100">--}}
{{--    </a>--}}
{{--    </div>--}}


@endsection

@section('styles')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css"/>
    <style>

        /*.btn-banner-home{*/
        /*    margin-top: 610%;*/
        /*}*/
        /*.btn{*/
        /*    font-weight: 500;*/
        /*}*/
        .header-gold{
            color: #ceae5e !important;
            font-size: 32px;
            font-weight: bold;
            padding-top:10%;
        }
        .txt-whymanuka{
            font-size: 26px;

        }
        .img-whymanuka{
            width:150px;
        }
        .header-whymanuka{
            font-color: white;
            font-size: 32px;
            padding-top:15%;
            padding-bottom: 5%;
            /*font-weight: lighter;*/
        }
        .bg-whymanuka {
            background-image: url('{{ asset('images/manuka/home/section-4-bg-mobile.png') }}');
            background-repeat: no-repeat;
            background-position: center bottom;
            background-size: cover;
            width: 100%;
            height: 1360px !important;
            /*position: relative;*/
            /*padding-top:100px;*/
        }
        .header-instagram{
            font-color: white;
            font-size: 32px;
            padding-bottom: 5%;
            /*font-weight: lighter;*/
        }
        .txt-product-home{
            font-size: 26px;
            font-weight: bold;
        }
        .border-product-home{
            border-bottom: 1px solid #ceae5e;
            width:80%;
            display: flow-root inline;
            display: inline-flex;
        }
        .img-product-home{
            width:300px;
        }
        .img-pure{
            width:100%;
        }
        .bg-home {
            background-image: url('{{ asset('images/manuka/home/banner-mobile.png') }}');
            background-repeat: no-repeat;
            background-position: center;
            background-size: cover;
            width: 100%;
            height: 286px !important;
            /*position: relative;*/
            /*top: 60px;*/
        }
        .bg-pure {
            background-image: url('{{ asset('images/manuka/home/section-2-bg-mobile.png') }}');
            background-repeat: no-repeat;
            background-position: center bottom;
            background-size: cover;
            width: 100%;
            height: 600px !important;
            /*position: relative;*/
            /*padding-top:100px;*/
        }
        {{--.bg-product {--}}
        {{--    background-image: url('{{ asset('images/manuka/home/section-2-bg-mobile.png') }}');--}}
        {{--    background-repeat: no-repeat;--}}
        {{--    background-position: center top;--}}
        {{--    background-size: cover;--}}
        {{--    width: 100%;--}}
        {{--}--}}
        /*.float{*/
        /*    position:fixed;*/
        /*    width:60px;*/
        /*    height:60px;*/
        /*    bottom:40px;*/
        /*    right:40px;*/
        /*    !*background-color:#38b91e;*!*/
        /*    color:#000;*/
        /*    border-radius:50px;*/
        /*    text-align:center;*/
        /*    !*box-shadow: 2px 2px 3px #999;*!*/
        /*    !*z-index: 99999999;*!*/
        /*}*/

        /*.my-float{*/
        /*    margin-top:0 px;*/
        /*}*/

        .modal-open .modal {
            display: flex!important;
            align-items: center!important;
            z-index: 99999999;
        }

        .modal-dialog {
            display: flex!important;
            align-items: center!important;
            flex-grow: 1;
            max-width: 320px;
            margin: 10px auto;
            overflow: visible;
        }

        .modal-header{
            border-bottom: 0 !important;
        }

        .bg_popup{;
            background-image: url('{{ asset('images/31ss/popup_special_payment.jpg') }}');
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center;
            height: 188px;
            width: 320px;
        }

        .slick-prev:before,
        .slick-next:before {
            color: sandybrown;
        }

        .slick-dots li.slick-active button:before{
            font-size: 14px;
            color: sandybrown;
        }

        .slick-dots li button:before{
            font-size: 14px;
            color: sandybrown;
        }

        .home-top-margin{
            margin-top: 0;
        }

        .img-banner-responsive{
            height: 100px;
        }

        .img-map-responsive{
            height: 250px;
        }

        #list_latest_new li{
            margin-bottom: 2rem;
        }

        #list_latest_new li:last-child{
            margin-bottom: 0;
        }

        #list_latest_new li::before{
            content: "\2022";  /* Add content: \2022 is the CSS Code/unicode for a bullet */
            color: #c38b63; /* Change the color */
            font-size: 25px;
            font-weight: bold; /* If you want it to be bold */
            line-height: 0.5rem;
            display: inline-block; /* Needed to add space between the bullet and the text */
            width: 1em; /* Also needed for space (tweak if needed) */
            margin-left: -1em; /* Also needed for space (tweak if needed) */
        }

        .img-fluid{
            max-width: 100%;
        }

        @media (max-width: 750px) {
            /*.img-product-home-1{*/
            /*    width:350px;*/
            /*    margin-left: -50px;*/
            /*}*/
            .bg-product {
                background-color: #1c1c1c;
            }
        }

        @media (min-width: 576px) {

        }

        @media (min-width: 750px) {


            .bg-product {
                background-image: url('{{ asset('images/manuka/home/01-Home-BG_03-fix.png') }}');
                background-repeat: no-repeat;
                background-position: center top;
                background-size: cover;
                width: 100%;
                height: 830px !important;
                /*position: relative;*/
                /*padding-top:100px;*/
                margin-top: -4%;
            }
            .btn-banner-home{
                margin-top:520%;
            }
            .img-pure{
                width:100%;
                /*margin-right:-15px;*/
                margin-top:-50px;
            }
            .header-whymanuka{
                font-color: white;
                font-size: 30px;
                padding-top:8%;
                padding-bottom: 5%;
                /*font-weight: lighter;*/
            }
            .img-whymanuka{
                width:200px;
            }
            .bg-whymanuka {
                background-image: url('{{ asset('images/manuka/home/01 home - bg green.png') }}');
                background-repeat: no-repeat;
                background-position: center bottom;
                background-size: cover;
                width: 100%;
                height: 760px !important;
                /*position: relative;*/
                /*padding-top:100px;*/
            }
            .bg-pure {
                background-image: url('{{ asset('images/manuka/home/01-Home-BG_02-fix.png') }}');
                background-repeat: no-repeat;
                background-position: center top;
                background-size: cover;
                width: 100%;
                height: 530px !important;
                /*position: relative;*/
                /*padding-top:100px;*/
                margin-bottom:-5px;
            }
            .header-gold{
                color: #ceae5e !important;
                font-size: 30px;
                padding-top:5%;
                font-weight: bold;
            }
            .bg-home {
                background-image: url('{{ asset('images/manuka/home/01-Home-BG_01-fix.png') }}');
                background-repeat: no-repeat;
                background-position: center;
                background-size: cover;
                width: 100%;
                height: 610px !important;
                position: relative;
                top: -5px;
                margin-bottom:-5px;
            }

            .header-instagram{
                font-color: white;
                font-size: 30px;
                padding-bottom: 5%;
                /*font-weight: lighter;*/
            }
            .modal-dialog {
                max-width: 700px;
                margin: 30px auto;
            }

            .bg_popup{
                height: 410px;
                width: 700px;
            }

            .img-fluid{
                max-width: 80%;
            }

            .home-top-margin{
                margin-top: -100px;
            }

            .img-banner-responsive{
                height: 700px;
            }

            .img-map-responsive{
                height: 650px;
            }
        }

        @media (min-width: 1200px) {

            .btn-banner-home{
                margin-top: 390%;
                margin-left: -25px;
            }

        }

        @media (min-width: 1300px) {

            .bg-product {
                background-image: url('{{ asset('images/manuka/home/01-Home-BG_03-fix.png') }}');
                background-repeat: no-repeat;
                background-position: center top;
                background-size: cover;
                width: 100%;
                height: 830px !important;
                /*position: relative;*/
                /*padding-top:100px;*/
                margin-top: -4%;
            }

            .btn-banner-home{
                margin-top:445%;
            }
            .img-pure{
                width:90%;
                /*margin-right:-15px;*/
                margin-top:-50px;
            }
            .header-whymanuka{
                font-color: white;
                font-size: 30px;
                padding-top:8%;
                padding-bottom: 5%;
                /*font-weight: lighter;*/
            }
            .img-whymanuka{
                width:200px;
            }
            .bg-whymanuka {
                background-image: url('{{ asset('images/manuka/home/01 home - bg green.png') }}');
                background-repeat: no-repeat;
                background-position: center bottom;
                background-size: cover;
                width: 100%;
                height: 760px !important;
                /*position: relative;*/
                /*padding-top:100px;*/
            }
            .bg-pure {
                background-image: url('{{ asset('images/manuka/home/01-Home-BG_02-fix.png') }}');
                background-repeat: no-repeat;
                background-position: center top;
                background-size: cover;
                width: 100%;
                height: 530px !important;
                /*position: relative;*/
                /*padding-top:100px;*/
                margin-bottom:-5px;
            }
            .bg-product {
                background-image: url('{{ asset('images/manuka/home/01-Home-BG_03-fix.png') }}');
                background-repeat: no-repeat;
                background-position: center top;
                background-size: cover;
                width: 100%;
                /*height: 600px !important;*/
                /*position: relative;*/
                /*padding-top:100px;*/
                margin-top: -4%;
            }
            .header-gold{
                color: #ceae5e !important;
                font-size: 30px;
                padding-top:5%;
                font-weight: bold;
            }
            .bg-home {
                background-image: url('{{ asset('images/manuka/home/01-Home-BG_01-fix.png') }}');
                background-repeat: no-repeat;
                background-position: center;
                background-size: cover;
                width: 100%;
                height: 760px !important;
                position: relative;
                top: -5px;
                margin-bottom:-5px;
            }
            .header-instagram{
                font-color: white;
                font-size: 30px;
                padding-bottom: 5%;
                /*font-weight: lighter;*/
            }
        }

        @media (min-width: 1400px) {
            .bg-pure {
                background-image: url('{{ asset('images/manuka/home/01-Home-BG_02-fix.png') }}');
                background-repeat: no-repeat;
                background-position: center top;
                background-size: cover;
                width: 100%;
                height: 620px !important;
                /*position: relative;*/
                /*padding-top:100px;*/
            }
            .bg-product {
                background-image: url('{{ asset('images/manuka/home/01-Home-BG_03-fix.png') }}');
                background-repeat: no-repeat;
                background-position: center top;
                background-size: cover;
                width: 100%;
                margin-top: -5%;
            }
            .btn-banner-home{
                margin-top:410%;
            }
        }
        @media (min-width: 1900px) {
            .padd-pure-fhd{
                padding-right:240px !important;
            }
            .btn-banner-home{
                margin-top:410%;
            }
            .bg-home {
                background-image: url('{{ asset('images/manuka/home/01-Home-BG_01-fix.png') }}');
                background-repeat: no-repeat;
                background-position: center bottom;
                background-size: cover;
                width: 100%;
                height: 985px !important;
                position: relative;
                top: -5px;
                margin-bottom:-5px;
            }
            .img-pure {
                width: 75%;
                /* margin-right: -15px; */
                margin-top: -65px;
            }
            .bg-pure {
                background-image: url('{{ asset('images/manuka/home/01-Home-BG_02-fix.png') }}');
                background-repeat: no-repeat;
                background-position: center top;
                background-size: cover;
                width: 100%;
                height: 750px !important;
                /* position: relative; */
                /* padding-top: 100px; */
            }
            .bg-product {
                background-image: url('{{ asset('images/manuka/home/01-Home-BG_03-fix.png') }}');
                background-repeat: no-repeat;
                background-position: center top;
                background-size: cover;
                width: 100%;
                margin-top: -6%;
            }
        }
    </style>
@endsection

@section('scripts')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>
    <script>

        $(".slider-five-centerpiece").slick({
            dots: true,
            infinite: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
        });

        $(".five-slider").slick({
            dots: true,
            infinite: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
        });

    </script>
@endsection
