@extends('layouts.frontend')

@section('head_and_title')
    <meta name="description" content="31 SUDIRMAN SUITES Search Results">
    <meta name="author" content="PT. Generasi Muda Gigih">
    <meta name="keywords" content="31 SUDIRMAN SUITES, Property, Office, Residence, Apartment, House">

    <title>MANUKA FARM - SEARCH RESULTS</title>
@endsection

@section('content')

    <section class="bg-blog pb-5 marmin-5">
        <div class="container-fluid">
            <div class="row no-gutters">
                <div class="col-12">
                </div>
            </div>
        </div>
    </section>

    <section class="py-5 bg-custom-dark">
        <div class="container">
            <div class="row mb-5">
                <div class="col-12">
                    <span class="t1-m-1 font-montserrat text-white">Berikut ini ditemukan {{ $results->count() }} hasil pencarian dari keyword "<span class="font-weight-bold">{{ $keyword }}</span>"</span>
                </div>
            </div>
            <div class="row">

                @if($results->count() === 0)
                    <div class="col-12 text-center">
                        <span class="t1-m-1 font-montserrat text-black">Hasil pencarian kosong..</span>
                    </div>
                @else
                    @foreach($results as $result)
                        <div class="col-sm-6 col-lg-4 p-b-50 isotope-item business-n">
                            <div class="bg-0 h-full">
                                <a href="{{ route('frontend.news_detail', ['slug' => $result->slug]) }}" class="hov-img0 of-hidden">
                                    <img src="{{ asset($result->img_path) }}" alt="IMG">
                                </a>

                                <div class="bg-custom-dark p-t-26">
                                    <h4 class="p-b-12">
                                        <a href="{{ route('frontend.news_detail', ['slug' => $result->slug]) }}" class="t1-m-1 cl-3 hov-link2 trans-02 font-montserrat font-weight-bold text-white">
                                            {{ $result->title }}
                                        </a>
                                    </h4>

                                    <div class="flex-wr-s-c p-b-9">
                                        <div class="p-r-20">
                                            <i class="fs-14 cl-7 fa fa-calendar m-r-2"></i>

                                            <span class="t1-s-2 cl-7 text-white font-montserrat">
										{{ $result->created_at_front_end_formatted }}
									</span>
                                        </div>

                                        <div class="p-l-20 bo-l-1 bcl-12">
                                            <i class="fs-14 cl-7 fa fa-user m-r-2"></i>

                                            <a href="#" class="t1-s-2 cl-11 hov-link3 text-white">
                                                {{ $result->createdBy->name }}
                                            </a>
                                        </div>
                                    </div>

                                    <p class="t1-s-2 cl-6 p-b-20 font-montserrat text-white">
                                        {{ $result->subtitle }}
                                    </p>

                                    <a href="{{ route('frontend.news_detail', ['slug' => $result->slug]) }}" class="d-inline-flex flex-c-c btn btn-gold  trans-02 gold font-montserrat">
                                        READ MORE
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif

            </div>
        </div>
    </section>
@endsection

@section('styles')
    <style>
        .bg-blog{
            background-image: url('{{ asset('images/manuka/blog/banner.png') }}');
            background-repeat: no-repeat;
            background-position: bottom;
            background-size: cover;
            width: 100%;
            height: 345px !important;
            /*margin-bottom:-5px;*/
        }
        .img-banner-responsive{
            height: 80px;
        }

        @media (min-width: 576px) {

        }

        @media (min-width: 768px) {

            .img-banner-responsive{
                height: 300px;
            }
        }

        @media (min-width: 992px) {

        }

        @media (min-width: 1200px) {
        }
        @media (min-width: 1900px) {
            .bg-blog{
                height:480px !important;
            }
        }
    </style>
@endsection

@section('scripts')
    <script>
    </script>
@endsection
