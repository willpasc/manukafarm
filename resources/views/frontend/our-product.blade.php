@extends('layouts.frontend')

@section('head_and_title')
    <meta name="description" content="31 SUDIRMAN SUITES Hotel">
    <meta name="author" content="PT. Generasi Muda Gigih">
    <meta name="keywords" content="31 SUDIRMAN SUITES, Property, Office, Residence, Apartment, House">

    <title>MANUKAFARM - OUR PRODUCTS</title>
@endsection

@section('content')
    <section class="bg-product marmin-5">
        <div class="container-fluid">
            <div class="row no-gutters">
                <div class="col-12">
                </div>
            </div>
        </div>
    </section>
    <section class="bg-custom-dark font-montserrat marbot-5">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <img src="{{ asset('images/manuka/product/mgs.png') }}" alt="img" class="mgs-logo-product">
                </div>
            </div>
            <div class="row">
                <div class="col-1 col-md-0"></div>
                <div class="col-md-12 col-10 text-center">
                    <p class="txt-body text-white font-weight-bold txt-product-home gold pb-md-2 pb-3">Manuka Farm Grading System - MGS (Molan Gold Standard)</p>
                    <p class="text-white txt-body px-md-5 px-2">is a system of assessing the level or levels of Methylglyoxal contained in Manuka Honey
                        to guarantee purity and quality. Molan Gold Standard was created by Dr. Peter Molan, who is one
                        of the pioneers of Manuka Honey and the leading honey expert in the world since 1981.</p>
                </div>
                <div class="col-1 col-md-0"></div>
            </div>
        </div>
    </section>

    <section class="bg-custom-dark py-5 font-montserrat marbot-5">
        <div class="container">
            <div class="row  pb-md-0 pb-5">
                <div class="col-md-6 text-center">
                    <img src="{{ asset('images/manuka/product/product-1.png') }}" alt="img" class="img-product px-5 px-md-0">
                </div>
                <div class="col-md-6 text-md-left text-center padd-prod-1">
                    <p class="pb-md-5 pb-1 txt-product-home ">
                        Manuka Farm<br> MGS 5+(250 gr)
                    </p>
                    <p class="txt-body font-montserrat text-white d-none d-md-block">
                        Contains more than 100  mg / kg of Methylglyoxal.
                    </p>
                    <p class="txt-body gold pt-md-3 pt-0 font-montserrat">
                        Maintenance (5+)
                    </p>
                    <hr class="border-bottom-gold">
                    <img src="{{ asset('images/manuka/product/anti-bacterial-1.png') }}" alt="img" class="anti-bacterial">
                    <br>
                    <a class="btn btn-gold font-montserrat-medium" href="https://naturalfarm.id/product/677/manuka-farm-mgs-5-250g">SHOP NOW</a>
                </div>
            </div>
            <div class="row pb-md-0 pb-5">
                <div class="col-12 d-block d-md-none">
                    <img src="{{ asset('images/manuka/product/product-2.png') }}" alt="img" class="img-product px-5 px-md-0">
                </div>
                <div class="col-2 d-none d-md-block">
                    &nbsp;
                </div>
                <div class="col-md-3 text-md-left text-center padd-prod-2">
                    <p class="pb-md-5 pb-3 txt-product-home ">
                        Manuka Farm<br> MGS 16+(250 gr)
                    </p>
                    <p class="txt-body font-montserrat text-white d-none d-md-block">
                        Contains more than 600  mg / kg of Methylglyoxal.
                    </p>
                    <p class="txt-body gold pt-md-3 pt-0 font-montserrat">
                        Specialized (16+)
                    </p>
                    <hr class="border-bottom-gold-1">
                    <img src="{{ asset('images/manuka/product/anti-bacterial-2.png') }}" alt="img" class="anti-bacterial">
                    <br>
                    <a class="btn btn-gold mt-4 font-montserrat-medium" href="https://naturalfarm.id/product/678/manuka-farm-mgs-16-250g">SHOP NOW</a>
                </div>
                <div class="col-md-7 d-none d-md-block text-center">
                    <img src="{{ asset('images/manuka/product/product-2.png') }}" alt="img" class="img-product px-5 px-md-0">
                </div>
            </div>
            <div class="row pb-md-0 pb-5">
                <div class="col-md-6 text-center">
                    <img src="{{ asset('images/manuka/product/product-3.png') }}" alt="img" class="img-product-3 px-5 px-md-0">
                </div>
                <div class="col-md-6 text-md-left text-center padd-prod-1">
                    <p class="pb-md-5 pb-3 txt-product-home ">
                        Manuka Farm<br> MGS 20+(250 gr)
                    </p>
                    <p class="txt-body font-montserrat text-white d-none d-md-block">
                        Contains more than 800  mg / kg of Methylglyoxal.
                    </p>
                    <p class="txt-body gold pt-md-3 pt-0 font-montserrat">
                        Therapeutic (20+)
                    </p>
                    <hr class="border-bottom-gold">
                    <img src="{{ asset('images/manuka/product/anti-bacterial-3.png') }}" alt="img" class="anti-bacterial">
                    <br>
                    <a class="btn btn-gold mt-4 font-montserrat-medium" href="https://naturalfarm.id/product/925/manuka-farm-mgs-20-250g">SHOP NOW</a>
                </div>
            </div>
        </div>
    </section>

@endsection


@section('styles')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css"/>
    <style>

        .img-product-3{
            width:100%;
        }
        .img-product{
            width:100%;
        }

        .padd-prod-1{
            padding-top:0px;
            padding-left:0px;
        }

        .padd-prod-2{
            padding-top:0px;
        }
        .mgs-logo-product{
            width:100%;
            padding-top:20%;
            padding-bottom:30px;
        }
        .txt-product-home{
            font-size: 26px;
            line-height: 1.5em;
            color: white;
            /*font-weight: bold;*/
        }
        .bg-product{
            background-image: url('{{ asset('images/manuka/product/banner-mobile.png') }}');
            background-repeat: no-repeat;
            background-position: bottom;
            background-size: cover;
            width: 100%;
            height: 345px !important;
        }
        .bg-header-developer {
            background-image: url('{{ asset('images/manuka/product/banner.png') }}');
            background-size: contain;
            background-repeat: no-repeat;
            background-position: bottom;
            height: 430px;
            width: auto;
        }
        .border-bottom-gold{
            border-bottom: 1px solid #ceae5e;
            margin-top: 0px;
            margin-bottom: 20px;
            width: 53%;
            display: inline-flex;
        }
        .border-bottom-gold-1{
            border-bottom: 1px solid #ceae5e;
            margin-top: 0px;
            margin-bottom: 20px;
            width: 86%;
            display: inline-flex;
        }
        .p-t-110{
            padding-top: 110px
        }
        .padding-20{
            padding-left: 20%;
            padding-right: 20%;
        }


        @media (min-width: 576px) {

        }

        @media (min-width: 768px) {
            .txt-product-home{
                font-size: 26px;
                line-height: 1.5em;
                color: #ceae5e;
                /*font-weight: bold;*/
            }
            .padd-prod-1{
                padding-top:100px;
                padding-left:140px;
            }

            .padd-prod-2{
                padding-top:80px;
            }
            .img-product-3{
                width:450px;
            }
            .img-product{
                width:500px;
            }
            .mgs-logo-product {
                width: 500px;
                padding-top: 5%;
                padding-bottom: 30px;
            }
            .bg-product{
                background-image: url('{{ asset('images/manuka/product/03 our products - header.png') }}');
                background-repeat: no-repeat;
                background-position: bottom;
                background-size: cover;
                width: 100%;
                height: 323px !important;
                margin-bottom:-5px;
            }
            .home-top-margin{
                margin-top: -100px;
            }

            .img-banner-responsive{
                height: 325px;
            }

            .img-map-responsive{
                height: 450px;
            }

            .gallery{
                height: 400px;
            }
            .tengah{
                margin-right: 140px;
            }
            .t1-b-1{
                font-size: 36px;
                line-height: 1.2;
                letter-spacing: 1px;
            }

            hr{
                margin-top: 1rem;
                margin-bottom: 1rem;
            }
        }

        @media (min-width: 992px) {

        }

        @media (min-width: 1200px) {
        }
        @media (min-width: 1900px) {
            .bg-product{
                height:480px !important;
            }
        }
    </style>
@endsection

@section('scripts')

@endsection
