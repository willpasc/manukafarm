@extends('layouts.frontend')

@section('head_and_title')
    <meta name="description" content="31 SUDIRMAN SUITES Hotel">
    <meta name="author" content="PT. Generasi Muda Gigih">
    <meta name="keywords" content="31 SUDIRMAN SUITES, Property, Office, Residence, Apartment, House">

    <title>MANUKAFARM - ABOUT US</title>
@endsection

@section('content')

    <section class="bg-about marmin-5">
        <div class="container-fluid">
            <div class="row no-gutters">
                <div class="col-12">
                </div>
            </div>
        </div>
    </section>
{{--    The gift desktop--}}
    <section class="bg-custom-dark py-5 d-none d-md-block font-montserrat marbot-5">
        <div class="container-fluid">
            <div class="row no-gutters">
                <div class="col-md-6 text-right">
                    <img src="{{ asset('images/manuka/aboutus/02 about - bee flower 2.png') }}" alt="img" class="img-gift">
                </div>
                <div class="col-md-6 text-md-left text-center txt-gift-padds">
                    <p class="pb-md-5 pb-3 header-gold font-montserrat">
                        The Gift From Nature
                    </p>
                    <p class="txt-body  text-white font-montserrat txt-thousand" style="position: relative;z-index: 10;">
                        For thousands of years honey has been used as a natural remedy.
                        Manuka Honey is a natural honey originating from the unique Manuka flower from New Zealand.
                        Unlike ordinary honey, Manuka Honey contains high anti-bacterial properties.
                        Methylglyoxal is a natural compound in Manuka honey that is directly responsible for the anti-bacterial properties of Manuka Honey
                    </p>
                </div>
            </div>
            <div class="row no-gutters pb-md-5 ">
                <div class="col-12 d-block d-md-none">
                    <img src="{{ asset('images/aboutus/02 about - bee flower.png') }}" alt="img" class="img-project">
                </div>

                <div class="col-md-5 text-white bg-madu padd-whymanuk">
                    <p class="header-gold pb-3 font-montserrat">
                        Why Manuka Farm?
                    </p>
                    <ul>
                    <div class="row pb-3">
                        <li>
                        <div class="col-1">
{{--                            <span class="bullet-why">&#9679;</span>--}}
                        </div>
                        <div class="col-11">
                            <p class="txt-body font-montserrat text-white">
                                Manuka Farm Manuka Honey is 100% pure New Zealand Manuka Honey with the highest quality.
                            </p>
                        </div>
                        </li>
                    </div>
                    <div class="row pb-3">
                        <div class="col-1">
{{--                            <span class="bullet-why">&#9679;</span>--}}
                        </div>
                        <li>
                        <div class="col-11">
                            <p class="txt-body font-montserrat text-white">
                                Manuka Honey Manuka Farm is obtained from one of the most natural and least polluted areas in New Zealand.
                            </p>
                        </div>
                        </li>
                    </div>
                    <div class="row">
                        <div class="col-1">
{{--                            <span class="bullet-why">&#9679;</span>--}}
                        </div>
                        <li>
                        <div class="col-11">
                            <p class="txt-body font-montserrat text-white">
                                Manuka Farm Manuka Honey has been tested and certified by the world quality institution ISO17025.
                            </p>
                        </div>
                        </li>
                    </div>
                    </ul>
                </div>
                <div class="col-md-6 d-none d-md-block text-right">
                    <img src="{{ asset('images/manuka/aboutus/02-about-us---beekeeper4-1.png') }}" alt="img" class="img-why-1">
                </div>
            </div>
        </div>
    </section>

{{--    The gift Mobile--}}

    <section class="bg-custom-dark pb-5 d-block d-md-none">
        <div class="container-fluid">
            <div class="row no-gutters text-center">
                <div class="col-1"></div>
                <div class="col-10">
                    <p class=" pb-3 pt-5 header-gold font-montserrat">
                        The Gift From Nature
                    </p>
                    <img src="{{ asset('images/manuka/aboutus/section-1-1-mobile.png') }}" alt="img" class="img-gift">
                    <p class="txt-body txt-gift-padds  text-white font-montserrat pt-5" style="position: relative;z-index: 10;">
                        For thousands of years honey has been used as a natural remedy.
                        Manuka Honey is a natural honey originating from the unique Manuka flower from New Zealand.
                        Unlike ordinary honey, Manuka Honey contains high anti-bacterial properties.
                        Methylglyoxal is a natural compound in Manuka honey that is directly responsible for the anti-bacterial properties of Manuka Honey
                    </p>
                </div>
                <div class="col-1"></div>
            </div>
            <div class="row no-gutters ">
                <div class="col-1"></div>
                <div class="col-10 d-block d-md-none">
                    <p class=" header-gold pb-3 pt-5 text-center font-montserrat">
                        Why Manuka Farm?
                    </p>
                    <img src="{{ asset('images/manuka/aboutus/section-1-2-mobile.png') }}" alt="img" class="w-100">
                    <ul class="pl-4 pt-3">
                    <div class="row no-gutters pb-3">
                        <div class="col-1">
{{--                            <span class="bullet-why">&#9679;</span>--}}
                        </div>
                        <li>
                        <div class="col-11">
                            <p class="txt-body font-montserrat text-white">
                                Manuka Farm Manuka Honey is 100% pure New Zealand Manuka Honey with the highest quality.
                            </p>
                        </div>
                        </li>
                    </div>
                    <div class="row no-gutters pb-3">
                        <div class="col-1">
{{--                            <span class="bullet-why">&#9679;</span>--}}
                        </div>
                        <li>
                        <div class="col-11">
                            <p class="txt-body font-montserrat text-white">
                                Manuka Honey Manuka Farm is obtained from one of the most natural and least polluted areas in New Zealand.
                            </p>
                        </div>
                        </li>
                    </div>
                    <div class="row no-gutters">
                        <div class="col-1">
{{--                            <span class="bullet-why">&#9679;</span>--}}
                        </div>
                        <li>
                        <div class="col-11">
                            <p class="txt-body font-montserrat text-white">
                                Manuka Farm Manuka Honey has been tested and certified by the world quality institution ISO17025.
                            </p>
                        </div>
                        </li>
                    </div>
                    </ul>
                </div>
                <div class="col-1"></div>
            </div>
        </div>
    </section>
    <section class="bg-custom-dark marbot-5">
        <div class="container">
            <div class="row no-pb-md-0 pb-5">
                <div class="col-md-5 col-12 text-center px-md-0 px-5">
                    <img src="{{ asset('images/manuka/aboutus/section-1-3.png') }}" alt="img" class="img-logo-mgs">
                </div>
                <div class="col-md-7 text-md-left text-center pr-md-5 bg-tawon px-md-0 px-5">
                    <p class="pb-3 pt-md-0 pt-4 header-gold font-montserrat">
                        Manuka Farm Grading System - <br/>MGS (Molan Gold Standard)
                    </p>
                    <p class="txt-body font-montserrat text-white">
                        is a system of assessing the level or levels of Methylglyoxal contained in
                        Manuka Honey to guarantee purity and quality. Molan Gold Standard was
                        created by Dr. Peter Molan, who is one of the pioneers of Manuka Honey
                        and the leading honey expert in the world since 1981.
                    </p>
                    <a href="https://www.mgs.org.nz/" class="btn btn-gold px-4 font-montserrat-medium">CLICK HERE TO LEARN MORE</a>
                </div>
            </div>
            <div class="row pb-5">
                <div class="col-12 text-center d-none d-md-block">
                        <img src="{{ asset('images/manuka/aboutus/tabel-mgs.png') }}" alt="logo" class="tabel-mgs ">
                </div>
                <div class="col-12 text-center d-block d-md-none">
                    <img src="{{ asset('images/manuka/aboutus/section-1-4.png') }}" alt="logo" class="tabel-mgs">
                </div>
            </div>
        </div>
    </section>

    <section class="pt-md-5 pt-0 bg-custom-dark marbot-5">
        <div class="container-fluid px-0">
            <div class="row no-gutters">
                <div class="col-md-12">
                    <div class="w-100 pt-5 img-banner-responsive d-none d-md-block girl-flower">
                    </div>
                    <div class="w-100 d-block d-md-none" style="background-image: url('{{ asset('images/manuka/aboutus/02 about - girl flower-m.png') }}');
                        background-repeat: no-repeat;
                        background-position: center;
                        background-size: cover;
                        height:240px;">
                    </div>
                </div>
            </div>
        </div>
    </section>


{{--    Prevent section desktop--}}
    <section class="bg-custom-dark pb-5 d-none d-md-block">
        <div class="container">
            <div class="row py-5">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-3 text-right">
                            <img src="{{ asset('images/manuka/aboutus/section-3-1.png') }}" alt="img" class="img-prevent">
                        </div>
                        <div class="col-md-7 text-left">
                            <p class="header-prevent pb-3 font-montserrat">
                                Prevents Infection
                            </p>
                            <p class="txt-body font-montserrat text-white">
                                Prevents infection when applied to wounds on the skin.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 ">
                    <div class="row">
                        <div class="col-md-3">
                            <img src="{{ asset('images/manuka/aboutus/section-3-4.png') }}" alt="img" class="img-prevent">
                        </div>
                        <div class="col-md-7 text-left">
                            <p class="header-prevent pb-3 font-montserrat">
                                Helps Digestive Problems
                            </p>
                            <p class="txt-body font-montserrat text-white">
                                Helps the digestion to be healthy.
                            </p>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                </div>
            </div>
            <div class="row  pb-5">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-3 text-right">
                            <img src="{{ asset('images/manuka/aboutus/section-3-2.png') }}" alt="img" class="img-prevent">
                        </div>
                        <div class="col-md-7 text-left">
                            <p class="header-prevent pb-3 font-montserrat">
                                Increase Endurance
                            </p>
                            <p class="txt-body font-montserrat text-white">
                                Helps body to build better immunity system.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 ">
                    <div class="row">
                        <div class="col-md-3">
                            <img src="{{ asset('images/manuka/aboutus/section-3-5.png') }}" alt="img" class="img-prevent">
                        </div>
                        <div class="col-md-7 text-left">
                            <p class="header-prevent pb-3 font-montserrat">
                                Increase Energy
                            </p>
                            <p class="txt-body font-montserrat text-white">
                                Helps body to have energy to do activities all day long.
                            </p>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                </div>
            </div>
            <div class="row pb-5">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-3 text-right">
                            <img src="{{ asset('images/manuka/aboutus/section-3-3.png') }}" alt="img" class="img-prevent">
                        </div>
                        <div class="col-md-7 text-left">
                            <p class="header-prevent pb-3 font-montserrat">
                                Prevents Sore Throat
                            </p>
                            <p class="txt-body font-montserrat text-white">
                                Overcoming sore throat and canker sores.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 ">
                    <div class="row">
                        <div class="col-md-3">
                            <img src="{{ asset('images/manuka/aboutus/section-3-6.png') }}" alt="img" class="img-prevent">
                        </div>
                        <div class="col-md-7 text-left">
                            <p class="header-prevent pb-3 font-montserrat">
                                Beauty Skin
                            </p>
                            <p class="txt-body font-montserrat text-white">
                                Beautify and smooth the skin, and also helps reducing inflammation.
                            </p>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>

{{--    Prevent Section Mobile--}}

    <section class="bg-custom-dark py-5 d-block d-md-none">
        <div class="container text-center">
            <div class="row">
                <div class="col-12">
                    <div class="pb-5">
                        <img src="{{ asset('images/manuka/aboutus/section-3-1.png') }}" alt="img" class="img-prevent">
                        <p class="header-prevent py-3 font-montserrat">
                            Prevents Infection
                        </p>
                        <p class="txt-body font-montserrat text-white px-5">
                            Prevents infection when applied to wounds on the skin.
                        </p>
                    </div>
                    <div class="pb-5">
                        <img src="{{ asset('images/manuka/aboutus/section-3-2.png') }}" alt="img" class="img-prevent">
                        <p class="header-prevent py-3 font-montserrat">
                            Increase Endurance
                        </p>
                        <p class="txt-body font-montserrat text-white px-5">
                            Helps body to build better immunity system.
                        </p>
                    </div>
                    <div class="pb-5">
                        <img src="{{ asset('images/manuka/aboutus/section-3-3.png') }}" alt="img" class="img-prevent">
                        <p class="header-prevent py-3 font-montserrat">
                            Prevents Sore Throat
                        </p>
                        <p class="txt-body font-montserrat text-white px-5">
                            Overcoming sore throat and canker sores.
                        </p>
                    </div>
                    <div class="pb-5">
                        <img src="{{ asset('images/manuka/aboutus/section-3-4.png') }}" alt="img" class="img-prevent">
                        <p class="header-prevent py-3 font-montserrat">
                            Helps Digestive Problems
                        </p>
                        <p class="txt-body font-montserrat text-white px-5">
                            Helps the digestion to be healthy.
                        </p>
                    </div>
                    <div class="pb-5">
                        <img src="{{ asset('images/manuka/aboutus/section-3-5.png') }}" alt="img" class="img-prevent">
                        <p class="header-prevent py-3 font-montserrat">
                            Increase Energy
                        </p>
                        <p class="txt-body font-montserrat text-white px-5">
                            Helps body to have energy to do activities
                            all day long.
                        </p>
                    </div>
                    <div>
                        <img src="{{ asset('images/manuka/aboutus/section-3-6.png') }}" alt="img" class="img-prevent">
                        <p class="header-prevent py-3">
                            Beauty Skin
                        </p>
                        <p class="txt-body font-montserrat text-white px-5">
                            Beautify and smooth the skin, and also
                            helps reducing inflammation.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="bg-findout">
        <div class="container-fluid px-0">
            <div class="row no-gutters">
                <div class="col-md-6"></div>
                <div class="col-md-2 col-12">
                    <div class="w-100 ">
                        <div class=" text-center px-3 font-montserrat-medium">
                            <a class="btn btn-gold font-montserrat-medium px-4 btn-knowmore" href="https://naturalfarm.id/"> KNOW MORE ABOUT OUR PRODUCERS
{{--                                <img src="{{ asset('images/manuka/aboutus/section-4-2-mobile.png') }}" alt="img" class="font-montserrat-medium btn-knowmore">--}}
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4"></div>
        </div>
    </section>


@endsection


@section('styles')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css"/>
    <style>
         li{
            list-style-type: disc;
             color:#ceae5e;
             font-size:20px;
        }
        .tabel-mgs{
            width:90%;
        }
        .bg-findout{
            background-image: url('{{ asset('images/manuka/aboutus/02 about -  white marble.png') }}');
            background-repeat: no-repeat;
            background-position: center top;
            background-size: cover;
            height:780px;
        }

        .btn-knowmore {
            width: 350px;
            margin-top: 195%;
        }

        .img-logo-mgs{
            width:80%;
        }
        .padd-whymanuk{
            padding-left: 113px !important;
            padding-top: 70px !important;
            padding-right: 30px !important;
        }
        .bullet-why{
            font-size: 30px !important;
            color: #ceae5e;
            position: relative;
            top:-10px;

        }
        .img-why-1{
            width: 100%;
            position: relative;
            margin-top: -90px;
            margin-left: 45px;

        }

        .bg-madu{
            background-image: url('{{ asset('images/manuka/aboutus/02-about---honey.png') }}');
            background-repeat: no-repeat;
            background-position: bottom;
            background-size: contain;
            width: 100%;
            height:400px;
        }

        .txt-gift-padds{
            padding-right: 0px !important;
            padding-left: 0px !important;
            padding-top: 0px;
        }
        .header-prevent{
            color: #ceae5e !important;
            font-size: 24px;
        }
        .header-gold{
            color: #ceae5e !important;
            font-size: 30px;
        }
        .container-fluid{
            padding:0px;
        }

        .img-gift{
            width: 100%;

        }

        .bg-about{
            background-image: url('{{ asset('images/manuka/aboutus/banner-mobile.png') }}');
            background-repeat: no-repeat;
            background-position: bottom;
            background-size: cover;
            width: 100%;
            height: 360px !important;
        }

        .t1-b-1{
            font-size: 20px;
            line-height: 1.2;
            letter-spacing: 1px;
        }

        hr{
            margin-top: 0.2rem;
            margin-bottom: 0.2rem;
        }
        .slick-prev:before,
        .slick-next:before {
            color: sandybrown;
        }

        .slick-dots li.slick-active button:before{
            font-size: 16px;
            color: sandybrown;
        }

        .slick-dots li button:before{
            font-size: 16px;
            color: sandybrown;
        }

        .home-top-margin{
            margin-top: 0;
        }

        .img-banner-responsive{
            height: 100px;
        }

        .img-map-responsive{
            height: 250px;
        }

        .gallery{
            height: 100px;
        }

        @media (min-width: 400px) {
            .bg-findout{
                background-image: url('{{ asset('images/manuka/aboutus/section-4-1-mobile.png') }}');
                background-repeat: no-repeat;
                background-position: center;
                background-size: cover;
                height:915px;
            }

        }

        @media (min-width: 768px) {

            .girl-flower{
                background-image: url('{{ asset('images/manuka/aboutus/02 about- girl flower.png') }}');
                background-repeat: no-repeat;
                background-position: center bottom;
                background-size: cover;
                height:450px;
            }

            .img-gift{
                width: 100%;

            }

            .tabel-mgs{
                width:75%;
            }
            .img-logo-mgs{
                width:100%;
            }
            .btn-knowmore{
                width: 350px;
                margin-top: 185%;
                padding-top: 0px;
                padding: 10px;
            }
            .bg-findout{
                background-image: url('{{ asset('images/manuka/aboutus/section-4.png') }}');
                background-repeat: no-repeat;
                background-position: center;
                background-size: cover;
                height:500px;
                margin-top:-35px;
            }

            .bg-tawon{
                background-image: url('{{ asset('images/manuka/aboutus/02-about---bee-hive2.png') }}');
                background-repeat: no-repeat;
                background-position: bottom;
                background-size: contain;
                width: 100%;
                height:360px;
            }
            .img-logo-mgs{
                width:300px;
            }

            .txt-gift-padds{
                padding-right: 150px !important;
                padding-left: 70px !important;
                padding-top: 65px;
            }
            .bg-about{
                background-image: url('{{ asset('images/manuka/aboutus/02 about - beader.png') }}');
                background-repeat: no-repeat;
                background-position: bottom;
                background-size: cover;
                width: 100%;
                height: 323px !important;
                /*margin-bottom:-5px;*/
            }
            .home-top-margin{
                margin-top: -100px;
            }

            .img-banner-responsive{
                height: 400px;
            }

            .img-map-responsive{
                height: 450px;
            }

            .gallery{
                height: 400px;
            }
            .tengah{
                margin-right: 140px;
            }
            .t1-b-1{
                font-size: 36px;
                line-height: 1.2;
                letter-spacing: 1px;
            }

            hr{
                margin-top: 1rem;
                margin-bottom: 1rem;
            }
        }

        @media (min-width: 992px) {

        }

        @media (min-width: 1300px) {
            .img-why-1{
                width: 100%;
                position: relative;
                margin-top: -145px;
                margin-left: 45px;

            }
            .txt-gift-padds{
                padding-right: 150px !important;
                padding-left: 70px !important;
                padding-top: 80px;
            }

            .btn-knowmore{
                width: 350px;
                margin-top: 170%;
                padding-top: 0px;
                padding: 10px;
            }
            .bg-findout{
                background-image: url('{{ asset('images/manuka/aboutus/section-4.png') }}');
                background-repeat: no-repeat;
                background-position: center;
                background-size: cover;
                height:500px;
                margin-top:-5px;
            }

            .bg-tawon{
                background-image: url('{{ asset('images/manuka/aboutus/02-about---bee-hive2.png') }}');
                background-repeat: no-repeat;
                background-position: bottom;
                background-size: contain;
                width: 100%;
                height:360px;
            }
            .img-logo-mgs{
                width:300px;
            }

            .txt-gift-padds{
                padding-right: 150px !important;
                padding-left: 70px !important;
                padding-top: 105px;
            }
            .bg-about{
                background-image: url('{{ asset('images/manuka/aboutus/02 about - beader.png') }}');
                background-repeat: no-repeat;
                background-position: bottom;
                background-size: cover;
                width: 100%;
                height: 346px !important;
                /*margin-bottom:-5px;*/
            }
            .home-top-margin{
                margin-top: -100px;
            }

            .img-banner-responsive{
                height: 400px;
            }

            .img-map-responsive{
                height: 450px;
            }

            .gallery{
                height: 400px;
            }
            .tengah{
                margin-right: 140px;
            }
            .t1-b-1{
                font-size: 36px;
                line-height: 1.2;
                letter-spacing: 1px;
            }

            hr{
                margin-top: 1rem;
                margin-bottom: 1rem;
            }
        }

         @media (min-width: 1400px){
             .girl-flower{
                 background-image: url('{{ asset('images/manuka/aboutus/02 about- girl flower.png') }}');
                 background-repeat: no-repeat;
                 background-position: center bottom;
                 background-size: cover;
                 height:500px;
             }
         }
        @media (min-width: 1900px) {
            .txt-gift-padds{
                padding-right: 150px !important;
                padding-left: 70px !important;
                padding-top: 130px;
            }
            .txt-thousand{
                padding-right:145px;
            }
            .girl-flower{
                background-image: url('{{ asset('images/manuka/aboutus/02 about- girl flower.png') }}');
                background-repeat: no-repeat;
                background-position: center bottom;
                background-size: cover;
                height:670px;
            }
            .bg-about{
                height:480px !important;
            }
            .img-why-1 {
                width: 90%;
                position: relative;
                margin-top: -263px;
                margin-left: 45px;
            }
            .img-gift {
                width: 75%;
                position: relative;
                left: -180px;
            }

            .bg-findout {
                background-image: url('{{ asset('images/manuka/aboutus/section-4.png') }}');
                background-repeat: no-repeat;
                background-position: center;
                background-size: cover;
                height: 700px;
            }
            .btn-knowmore {
                width: 400px;
                margin-top: 165%;
                margin-left: 30px;
            }
        }

        /*dat apple device*/


    </style>
@endsection

@section('scripts')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>
    <script>

        $(".slider-five-centerpiece").slick({
            dots: true,
            infinite: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
        });

        $(".five-slider").slick({
            dots: true,
            infinite: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
        });
    </script>
@endsection
