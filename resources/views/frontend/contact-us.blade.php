@extends('layouts.frontend')

@section('head_and_title')
    <meta name="description" content="31 SUDIRMAN SUITES Contact Us">
    <meta name="author" content="PT. Generasi Muda Gigih">
    <meta name="keywords" content="31 SUDIRMAN SUITES, Property, Office, Residence, Apartment, House">

    <title>MANUKAFARM - CONTACT US</title>
@endsection

@section('content')

    <!-- About -->

    <section class="bg-contact marmin-5">
        <div class="container-fluid">
            <div class="row no-gutters">
                <div class="col-12">
                </div>
            </div>
        </div>
    </section>

    <section class="bg-contact-1 font-montserrat marbot-5">
        <div class="container text-center">
            <div class="row mb-4">
                <div class="col-12">
                    <p class="header-gold">
                        Contact Us
                    </p>
                    <p class=" text-white font-weight-bold">
                        Leave us a Message
                    </p>
                    <p class="text-white">
                        Any question? Do not hesitate to ask!
                    </p>
                </div>
            </div>

            {{ Form::open(['route'=>['frontend.contact_us.save'],'method' => 'post','id' => 'contact-form', 'class'=>'validate-form']) }}

            <div class="row mb-12">
                <div class="col-md-3 d-none d-md-block"></div>
                <div class="col-md-6 col-12 mb-4 mb-md-0 text-center">
                    @if(count($errors))
                        <div class="row mb-4">
                            <div class="col-12">
                                <div role="alert" class="alert alert-danger">
                                    <h4 class="font-custom-tiempos-medium">Terdapat kesalahan isi pesan</h4>
                                    <br/>
                                    @foreach($errors->all() as $error)
                                        <li><span class="font-custom-tiempos-light">{{ $error }}</span></li>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @endif

                    @if(\Illuminate\Support\Facades\Session::has('success'))
                        <div class="row mb-4">
                            <div class="col-12">
                                <div role="alert" class="alert alert-success">
                                    <span class="t1-m1-1 font-custom-tiempos-medium">{{ \Illuminate\Support\Facades\Session::get('success') }}</span>
                                </div>
                            </div>
                        </div>
                    @endif

                    <div class="row pb-4">
                        <div class="col-6">
                            <p class="text-white text-left pb-2">Name</p>
                            <input type="text" class="form-control" id="name" name="name" placeholder="Your Name" required/>
                        </div>
                        <div class="col-6">
                            <p class="text-white text-left pb-2">Phone</p>
                            <input type="text" class="form-control" id="phone" name="phone" placeholder="Your Phone Number" required/>
                        </div>
                    </div>
                    <div class="row pb-4">
                        <div class="col-6">
                            <p class="text-white text-left pb-2">Email</p>
                            <input type="text" class="form-control" id="email" name="email" placeholder="Your Email" required/>

                        </div>
                        <div class="col-6">
                            <p class="text-white text-left pb-2">Subject</p>
                            <input type="text" class="form-control" id="subject" name="subject" placeholder="Your Subject"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <p class="text-white text-left pb-2">Your Message</p>
                            <textarea type="text" class="form-control" id="message" name="message" rows="10" placeholder="Your Message"></textarea>
                        </div>
                    </div>
                    <div class="row d-block d-md-none mt-3 mt-md-0">
                        <div class="col-12">
                            <input type="submit" class="btn btn-lg btn-gold px-4 font-montserrat-medium" style="cursor: pointer;" value="SEND MESSAGE" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="row pb-5 d-none d-md-block">
                <div class="col-12">
                    <input type="submit" class="btn btn-lg btn-gold px-4 font-montserrat-medium" style="cursor: pointer;" value="SEND MESSAGE" />
                </div>
            </div>

            {{ Form::close() }}

        </div>
    </section>

@endsection

@section('styles')
    <style>

        .bg-contact-1{
            background-image: url('{{ asset('images/manuka/contactus/04 contact -bg flower.png') }}');
            background-repeat: no-repeat;
            background-position: bottom;
            background-size: cover;
            width: 100%;
            height: 760px !important;
        }

        .header-gold{
            color: #ceae5e !important;
            font-size: 30px;
            font-weight: bold;
            padding-top: 15%;
            padding-bottom: 2%;
        }

        .bg-contact{
            background-image: url('{{ asset('images/manuka/contactus/banner-mobile.png') }}');
            background-repeat: no-repeat;
            background-position: bottom;
            background-size: cover;
            width: 100%;
            height: 360px !important;
        }

        .t1-b-1{
            font-size: 20px;
            line-height: 1.2;
            letter-spacing: 1px;
        }

        hr{
            margin-top: 0.2rem;
            margin-bottom: 0.2rem;
        }
        .btn-dark{
            border-radius: 30%;
        }
        .text-contact-size{
            font-size: 15px;
        }

        #contact-form .form-control{
            background-color: #f6f7f7;
            border: 0;
            padding: 1rem;
            border-radius: 15px;
        }

        #contact-form input::placeholder, #contact-form textarea::placeholder{
            color: #000;
            font-size: 11px;
            font-family: 'montserratregular', sans-serif !important;
        }

        .img-banner-responsive{
            height: 100px;
        }

        @media (max-width: 576px) {
            .centered {
                position: absolute;
                top: 80%;
                left: 50%;
                transform: translate(-50%, -50%);
                min-width: 300px;
            }
        }

        @media (min-width: 768px) {

            .bg-contact-1{
                background-image: url('{{ asset('images/manuka/contactus/04 contact -bg flower.png') }}');
                background-repeat: no-repeat;
                background-position: bottom;
                background-size: cover;
                width: 100%;
                height: 860px !important;
            }

            .header-gold{
                color: #ceae5e !important;
                font-size: 30px;
                padding-top: 7%;
                font-weight: bold;
                padding-bottom: 2%;
            }

            .bg-contact{
                background-image: url('{{ asset('images/manuka/contactus/04 contact - header.png') }}');
                background-repeat: no-repeat;
                background-position: bottom;
                background-size: cover;
                width: 100%;
                height: 323px !important;
                /*margin-bottom:-5px;*/
            }

            .centered {
                position: absolute;
                top: 40%;
                left: 50%;
                transform: translate(-50%, -50%);
                min-width: 300px;
            }

            .img-banner-responsive{
                height: 325px;
            }
            .t1-b-1{
                font-size: 36px;
                line-height: 1.2;
                letter-spacing: 1px;
            }

            hr{
                margin-top: 1rem;
                margin-bottom: 1rem;
            }
        }

        @media (max-width: 992px) {
        }

        @media (min-width: 1024px) {

            .centered {
                position: absolute;
                top: 40%;
                left: 50%;
                transform: translate(-50%, -50%);
                min-width: 300px;
            }
        }

        @media (min-width: 1200px) {
            .centered {
                position: absolute;
                top: 45%;
                left: 50%;
                transform: translate(-50%, -50%);
                min-width: 300px;
            }
        }
        @media (min-width: 1900px) {
            .bg-contact{
                height:480px !important;
            }
        }
    </style>
@endsection

@section('scripts')
    <script>
    </script>
@endsection
