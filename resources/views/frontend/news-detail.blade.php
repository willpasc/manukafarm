@extends('layouts.frontend')

@section('head_and_title')
    <meta name="description" content="31 SUDIRMAN SUITES News Detail">
    <meta name="author" content="PT. Generasi Muda Gigih">
    <meta name="keywords" content="31 SUDIRMAN SUITES, Property, Office, Residence, Apartment, House">

    <title>MANUKAFARM - BLOGS DETAIL</title>
@endsection

@section('content')

    <section class="bg-blog pb-5 marmin-5">
        <div class="container-fluid">
            <div class="row no-gutters">
                <div class="col-12">
                </div>
            </div>
        </div>
    </section>

<section class="bg-custom-dark pt-5 marbot-5">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-12">
                <div class="row mb-4">
                    <div class="col-1"></div>
                    <div class="col-9">
                        <div class="mb-3">
                            <span class=" font-montserrat" style="color:white; font-size:12px;">Date, {{ $article->created_at_front_end_formatted }}</span><br/>
                        </div>
                        <div class="mb-3">
                            <span class="t1-m-1 font-montserrat gold">{{ $article->title }}</span><br/><br/>
                            <span class="t1-m-2 font-montserrat text-white">{{ $article->subtitle ?? "" }}</span>
                        </div>
                    </div>
                </div>
                <div class="row mb-2">
                    <div class="col-md-1"></div>
                    <div class="col-md-10 col-12 mb-4 mb-md-0 text-center text-md-left">
                        <img class="pb-5" src="{{ asset($article->img_path) }}" style="height: 400px;"/>
                        <div class=" font-montserrat text-white">
                            {!! $article->description !!}
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                </div>
            </div>
            <div class="col-md-3 col-12 pt-4">
                <div class="row">
                    <div class="col-12">
                        <span class="t1-m-4 font-montserrat text-white">News</span>
                        <div class="size-a-1 bg-3" style="width: 50%; height: 1px;"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 font-montserrat">
                        <ul id="treeview">
                            @foreach( $years as $year => $groupedNews)
                                <li>{{ $year }}
                                    <ul>
                                        @foreach($groupedNews as $groupedNew)
                                            <li><a href="{{ route('frontend.news_detail', ['slug' => $groupedNew->slug]) }}">{{ $groupedNew->title }}</a></li>
                                        @endforeach
                                    </ul>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>

{{--                <span>2019  ></span><br/>--}}
{{--                <span>2018  v</span><br/>--}}
{{--                <div class="col-md-1"></div>--}}
{{--                <div class="col" style="font-size:12px; color:grey">--}}
{{--                    <span>Lorem ipsum dolor sit</span><br/>--}}
{{--                    <span>Consectetuer Adipiscing</span>--}}
{{--                </div>--}}
            </div>
        </div>
    </div>
</section>

<section class="bg-custom-dark pb-5 marbot-5">
    <div class="container">
        <div class="row">
            <hr class="w-100" style="border-bottom: 1px solid #ceae5e;"/>
        </div>
    </div>
</section>

<section class="pb-5 bg-custom-dark marbot-5">
        <div class="container">
            <div class="row isotope-grid mb-4">
                <div class="col-md-3">
                    <div>
                        <span class="font-weight-bold font-montserrat gold" style="font-size: 20px;">Further Reading</span>
                    </div>
                </div>
            </div>
            <div class="row">
                <section class="mb-5">
                    <div class="container">
                        <div class="row">
                            @foreach($news as $new)
                                <div class="col-sm-6 col-lg-4 p-b-50">
                                    <div class="bg-custom-dark h-full">
                                        <a href="{{ route('frontend.news_detail', ['slug' => $new->slug]) }}" class="hov-img0 of-hidden">
                                            <img src="{{ asset($new->img_path) }}" alt="IMG">
                                        </a>

                                        <div class="bg-custom-dark p-t-26">
                                            <h4 class="p-b-12">
                                                <a href="{{ route('frontend.news_detail', ['slug' => $new->slug]) }}" class="t1-m-1 cl-3 hov-link2 trans-02 font-montserrat gold">
                                                    {{ $new->title }}
                                                </a>
                                                <div class="py-2">
                                                    <div class="bg-3" style="width: 15%; height: 1px;background-color: #ceae5e;"></div>
                                                </div>
                                            </h4>

                                            <p class="t1-s-2 cl-6 p-b-20 font-montserrat text-white">
                                                {{ $new->subtitle }}
                                            </p>

                                            {{-- <a href="news-detail.html" class="d-inline-flex flex-c-c size-a-1 p-rl-15 t1-s-2 text-uppercase cl-0 bg-11 hov-btn1 trans-02">
                                                Read More
                                            </a> --}}
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
@endsection

@section('styles')
{{--    <link rel="stylesheet" type="text/css" href="http://www.shieldui.com/shared/components/latest/css/light-bootstrap/all.min.css" />--}}
    <link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/all.min.css') }}" />

    <style>

        .bg-blog{
            background-image: url('{{ asset('images/manuka/blog/06 blog - header.png') }}');
            background-repeat: no-repeat;
            background-position: bottom;
            background-size: cover;
            width: 100%;
            height: 323px !important;
            /*margin-bottom:-5px;*/
        }
        .t1-b-1{
            font-size: 20px;
            line-height: 1.2;
            letter-spacing: 1px;
        }

        hr{
            margin-top: 0.2rem;
            margin-bottom: 0.2rem;
        }
        .slick-prev:before,
        .slick-next:before {
            color: sandybrown;
        }

        .slick-dots li.slick-active button:before{
            font-size: 16px;
            color: sandybrown;
        }

        .slick-dots li button:before{
            font-size: 16px;
            color: sandybrown;
        }

        .home-top-margin{
            margin-top: 0;
        }

        .img-banner-responsive{
            height: 80px;
        }

        .img-map-responsive{
            height: 250px;
        }

        @media (max-width: 576px) {
            .bg-blog{
                height:300px !important;
            }
        }

        @media (min-width: 768px) {
            .home-top-margin{
                margin-top: -100px;
            }

            .img-banner-responsive{
                height: 300px;
            }

            .img-map-responsive{
                height: 650px;
            }
            .t1-b-1{
                font-size: 36px;
                line-height: 1.2;
                letter-spacing: 1px;
            }

            hr{
                margin-top: 1rem;
                margin-bottom: 1rem;
            }
        }

        @media (min-width: 992px) {

        }

        @media (min-width: 1200px) {
        }
        @media (min-width: 1900px) {
            .bg-blog{
                height:530px !important;
            }
        }
    </style>
@endsection

@section('scripts')
{{--    <script type="text/javascript" src="http://www.shieldui.com/shared/components/latest/js/shieldui-all.min.js"></script>--}}
    <script type="text/javascript" src="{{ asset('js/frontend/shieldui-all.min.js') }}"></script>
    <script>
        jQuery(function ($) {
            $("#treeview").shieldTreeView();
        });
    </script>
@endsection
