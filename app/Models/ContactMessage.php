<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 11 Apr 2019 04:14:33 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ContactMessageMail
 *
 * @property int $id
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $subject
 * @property string $message
 * @property \Carbon\Carbon $created_at
 *
 * @package App\Models
 */
class ContactMessage extends Eloquent
{
	public $timestamps = false;

	protected $fillable = [
		'name',
        'phone',
		'email',
		'subject',
		'message',
        'created_at'
	];
}
