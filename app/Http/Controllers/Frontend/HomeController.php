<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Mail\ContactMessageMail;
use App\Models\Blog;
use App\Models\ContactMessage;
use App\Models\Subscribe;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = Blog::where('status_id', 4)
            ->orderBy('created_at', 'desc')
            ->take(3)
            ->get();

        return view('frontend.home', compact('news'));
    }

    public function about()
    {
        return view('frontend.about-us');
    }

    public function product()
    {
        return view('frontend.our-product');
    }

    public function contactUs()
    {
        return view('frontend.contact-us');
    }

    public function news()
    {
        $news = Blog::where('status_id', 4)
            ->orderBy('created_at', 'desc')
            ->get();

        return view('frontend.news', compact('news'));
    }


    public function newsdetail($slug)
    {
        $new = Blog::where('slug', $slug)->first();
        //dd($new);
        if(empty($new)){
            return redirect()->back();
        }

        $news = Blog::where('status_id', 4)
            ->inRandomOrder()
            ->take(3)
            ->get();

        $years = Blog::where('status_id', 4)
            ->orderBy('created_at', 'desc')
            ->get()
            ->groupBy(function ($val) {
                return Carbon::parse($val->created_at)->format('Y');
            });

        $data = [
            'article'       => $new,
            'news'          => $news,
            'years'         => $years
        ];

        return view('frontend.news-detail')->with($data);
    }

    public function saveContactUs(Request $request){
        try{
            $validator = Validator::make($request->all(),[
                'name'          => 'required|max:50',
                'phone'         => 'required|max:25',
                'email'         => 'required|email|max:50',
                'subject'       => 'required|max:50',
                'message'       => 'required|max:250'
            ],[
                'name.required'         => 'Nama wajib diisi!',
                'phone.required'        => 'Nomor Ponsel wajib diisii!',
                'email.required'        => 'Email wajib diisii!',
                'subject.required'      => 'Subject wajib diisii!',
                'message.required'      => 'Message wajib diisii!',
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }

            $now = Carbon::now('Asia/Jakarta');

            $newMessage = ContactMessage::create([
                'name'          => $request->input('name'),
                'phone'         => $request->input('phone'),
                'email'         => $request->input('email'),
                'subject'       => $request->input('subject'),
                'message'       => $request->input('message'),
                'created_at'    => $now->toDateTimeString(),
            ]);
//            dd("meongs");

//            Mail::to("info@31sudirmansuites.com")->send(new ContactMessageMail($newMessage));
            Session::flash('success', 'Pesan Anda berhasil diterima!');

            return redirect()->route('frontend.contact_us');
        }
        catch (\Exception $ex){
            Log::error("HomeController - saveContactUs error: ". $ex);
        }
    }

    //=================================== 31sudirman code ===================================

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexs()
    {
        $news = Blog::where('status_id', 4)
            ->orderBy('created_at', 'desc')
            ->take(2)
            ->get();

        return view('frontend.home', compact('news'));
    }

    public function privilegecard()
    {
        return view('frontend.privilegecard');
    }

    public function apartments()
    {
        return view('frontend.apartments');
    }

    public function hotel()
    {
        return view('frontend.hotel');
    }

    public function gallery()
    {
        return view('frontend.gallery');
    }

    public function partners(){
        return view('frontend.partners');
    }

    public function location(){
        return view('frontend.location');
    }

    public function contactUss(){

        return view('frontend.contact-us');
    }

    public function saveSubscribe(Request $request){

        $now = Carbon::now('Asia/Jakarta');

        $newSubscribe = Subscribe::create([
            'email'         => $request->input('email'),
            'created_at'    => $now->toDateTimeString(),
        ]);

        return Response::json(array('success' => 'VALID'));
    }

    public function downloadCatalogue(){
        return response()->download(public_path('marc-catalogue.pdf'));
    }
}
